from graphics import *
from time import sleep
from .constantes.cores import Cores
from .constantes.plano import Y_MAX, X_MAX
from .constantes.coordenadas import coord
from .constantes.fundo import fundo
from .primitivas.aviao import Aviao


def main():
    radar = GraphWin("Radar", X_MAX, Y_MAX, autoflush=False)
    radar.setBackground(Cores["planoDeFundo"])

    counter = 0

    for t in coord:
        print(counter)
        counter += 1
        radar.delete("all")
        fundo(radar)
        for key in t:
            cod = key
            direcao = t[key][0]
            x = t[key][1]
            y = t[key][2]
            z = t[key][3]

            Aviao(radar, x, y, z, 10000, direcao, cod)
            print("Aviao " + key + "colocado no mapa")
        sleep(0.7)

        radar.getMouse()

    return
