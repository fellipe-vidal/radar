from graphics import Text, Point
from ..constantes.plano import Y_MAX, X_MAX


def Texto(win, x, y, palavra, cor, tamanho, estilo):
    x = x + X_MAX/2
    y = Y_MAX/2 - y

    t = Text(Point(x, y), palavra)

    t.setOutline(cor)
    t.setSize(tamanho)
    t.setStyle(estilo)
    t.draw(win)

    return
