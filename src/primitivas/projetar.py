import math


def Projetar(win, x, y, z, p):
    F = 30 * p

    x1 = x * p/(F - z)
    y1 = y * p/(F - z)

    return (math.trunc(x1), math.trunc(y1))
