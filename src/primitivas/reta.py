from .ponto import Ponto
import math


def Reta(win, x1, y1, x2, y2, cor, espessura, tipo):
    contador = 0

    x = x1
    y = y1
    p = 0
    dx = x2-x1
    dy = y2-y1
    xInc = 1
    yInc = 1

    if dx < 0:
        xInc = -1
        dx = -dx
    if dy < 0:
        yInc = -1
        dy = -dy

    if dy <= dx:
        p = dx/2
        while x != x2:
            if tipo == "continua":
                Ponto(win, x, y, cor, espessura)

            if tipo == "pontilhada":
                if contador % 10 == 0:
                    Ponto(win, x, y, cor, espessura)
                contador = contador + 1

            if tipo == "tracejada":
                if contador < 5:
                    Ponto(win, x, y, cor, espessura)
                contador = contador + 1
                if contador == 10:
                    contador = 0

            p = p-dy
            if p < 0:

                y = y+yInc
                p = p+dx

            x = x+xInc

    else:

        p = dy/2
        while y != y2:
            Ponto(win, x, y, cor, espessura)
            p = p-dx
            if p < 0:

                x = x+xInc
                p = p+dy

            y = y+yInc
        Ponto(win, x, y, cor, espessura)

    return
