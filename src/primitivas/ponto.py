from ..constantes.plano import Y_MAX, X_MAX


def Ponto(win, x, y, cor, tamanho):

    x = x + X_MAX/2
    y = Y_MAX/2 - y

    if tamanho == 1:
        win.plotPixel(x, y, cor)

    if tamanho == 2:
        win.plotPixel(x, y, cor)
        win.plotPixel(x+1, y, cor)
        win.plotPixel(x, y-1, cor)
        win.plotPixel(x+1, y-1, cor)

    if tamanho == 3:
        win.plotPixel(x, y, cor)
        win.plotPixel(x, y+1, cor)
        win.plotPixel(x+1, y, cor)
        win.plotPixel(x, y-1, cor)
        win.plotPixel(x-1, y, cor)

    if tamanho == 4:
        win.plotPixel(x, y, cor)
        win.plotPixel(x+1, y, cor)
        win.plotPixel(x, y-1, cor)
        win.plotPixel(x+1, y-1, cor)
        win.plotPixel(x, y+1, cor)
        win.plotPixel(x+1, y+1, cor)
        win.plotPixel(x+2, y, cor)
        win.plotPixel(x+2, y-1, cor)
        win.plotPixel(x+1, y-2, cor)
        win.plotPixel(x, y-2, cor)
        win.plotPixel(x-1, y-1, cor)
        win.plotPixel(x-1, y, cor)

    return
