from .projetar import Projetar
from .ponto import Ponto
from .reta import Reta
from .texto import Texto


def Aviao(win, X, Y, z, P, direcao, palavra):

    cor = "white"

    x = Projetar(win, X, Y, z, P)[0]
    y = Projetar(win, X, Y, z, P)[1]

    if direcao == "D":
        cor = "green"

    if direcao == "P":
        cor = "red"

    if x == 0 and y == 0:
        Reta(win, x, y, x - 20, y, cor, 2, "continua")
        Ponto(win, x, y, 'red', 2)
        Reta(win, x - 6, y + 8, x - 6, y - 8, cor, 2, "continua")
        Reta(win, x - 16, y + 4, x - 16, y - 4, cor, 2, "continua")

    if x == 0:
        angulo = 4
    else:
        angulo = y/x

    if angulo >= 3.73:
        if y > 0:
            if direcao == 'D':
                Texto(win, x + 20, y + 5, palavra, cor, 10, 'bold')
                Reta(win, x, y, x, y - 20, cor, 2, "continua")
                Ponto(win, x, y, 'red', 2)
                Reta(win, x + 4, y - 15, x - 4, y - 15, cor, 2, "continua")
                Reta(win, x + 8, y - 5, x - 8, y - 5, cor, 2, "continua")
            if direcao == 'P':
                Texto(win, x + 20, y + 5, palavra, cor, 10, 'bold')
                Reta(win, x, y, x, y - 20, cor, 2, "continua")
                Ponto(win, x, y - 20, 'red', 2)
                Reta(win, x + 4, y - 3, x - 4, y - 3, cor, 2, "continua")
                Reta(win, x + 8, y - 13, x - 8, y - 13, cor, 2, "continua")
        if y < 0:
            if direcao == 'D':
                Texto(win, x + 20, y + 5, palavra, cor, 10, 'bold')
                Reta(win, x, y, x, y - 20, cor, 2, "continua")
                Ponto(win, x, y - 20, 'red', 2)
                Reta(win, x + 4, y - 3, x - 4, y - 3, cor, 2, "continua")
                Reta(win, x + 8, y - 13, x - 8, y - 13, cor, 2, "continua")
            if direcao == 'P':
                Texto(win, x + 20, y + 5, palavra, cor, 10, 'bold')
                Reta(win, x, y, x, y - 20, cor, 2, "continua")
                Ponto(win, x, y, 'red', 2)
                Reta(win, x + 4, y - 15, x - 4, y - 15, cor, 2, "continua")
                Reta(win, x + 8, y - 5, x - 8, y - 5, cor, 2, "continua")
        return
    if 0.28 >= angulo >= -0.27:
        if x > 0:
            if direcao == 'D':
                Texto(win, x + 20, y + 5, palavra, cor, 10, 'bold')
                Reta(win, x, y, x - 20, y, cor, 2, "continua")
                Ponto(win, x, y, 'red', 2)
                Reta(win, x - 6, y + 8, x - 6, y - 8, cor, 2, "continua")
                Reta(win, x - 16, y + 4, x - 16, y - 4, cor, 2, "continua")
            if direcao == 'P':
                Texto(win, x + 20, y + 5, palavra, cor, 10, 'bold')
                Reta(win, x, y, x - 20, y, cor, 2, "continua")
                Ponto(win, x - 20, y, 'red', 2)
                Reta(win, x - 3, y + 4, x - 3, y - 4, cor, 2, "continua")
                Reta(win, x - 13, y + 8, x - 13, y - 8, cor, 2, "continua")
        if x < 0:
            if direcao == 'P':
                Texto(win, x + 20, y + 5, palavra, cor, 10, 'bold')
                Reta(win, x, y, x - 20, y, cor, 2, "continua")
                Ponto(win, x, y, 'red', 2)
                Reta(win, x - 6, y + 8, x - 6, y - 8, cor, 2, "continua")
                Reta(win, x - 16, y + 4, x - 16, y - 4, cor, 2, "continua")
            if direcao == 'D':
                Texto(win, x + 20, y + 5, palavra, cor, 10, 'bold')
                Reta(win, x, y, x - 20, y, cor, 2, "continua")
                Ponto(win, x - 20, y, 'red', 2)
                Reta(win, x - 3, y + 4, x - 3, y - 4, cor, 2, "continua")
                Reta(win, x - 13, y + 8, x - 13, y - 8, cor, 2, "continua")
        return
    if 3.73 > angulo > 0.28:
        if x > 0:
            if direcao == 'D':
                Texto(win, x + 20, y + 5, palavra, cor, 10, 'bold')
                Reta(win, x, y, x - 12, y - 12, cor, 2, "continua")
                Ponto(win, x, y, 'red', 2)
                Reta(win, x - 12, y - 5, x - 3, y - 14, cor, 2, "continua")
                Reta(win, x + 3, y - 10, x - 10, y + 4, cor, 2, "continua")
            if direcao == 'P':
                Texto(win, x + 20, y + 5, palavra, cor, 10, 'bold')
                Reta(win, x, y, x - 12, y - 12, cor, 2, "continua")
                Ponto(win, x - 12, y - 12, 'red', 2)
                Reta(win, x - 15, y - 2, x - 1, y - 15, cor, 2, "continua")
                Reta(win, x + 1, y - 5, x - 5, y + 2, cor, 2, "continua")
        if x < 0:
            if direcao == 'D':
                Texto(win, x + 20, y + 5, palavra, cor, 10, 'bold')
                Reta(win, x, y, x - 12, y - 12, cor, 2, "continua")
                Ponto(win, x-12, y-12, 'red', 2)
                Reta(win, x - 15, y - 2, x - 1, y - 15, cor, 2, "continua")
                Reta(win, x + 1, y - 5, x - 5, y + 2, cor, 2, "continua")
            if direcao == 'P':
                Texto(win, x + 20, y + 5, palavra, cor, 10, 'bold')
                Reta(win, x, y, x - 12, y - 12, cor, 2, "continua")
                Ponto(win, x, y, 'red', 2)
                Reta(win, x - 12, y - 5, x - 3, y - 14, cor, 2, "continua")
                Reta(win, x + 3, y - 10, x - 10, y + 4, cor, 2, "continua")
        return
    if -0.27 >= angulo >= -3.73:
        if x > 0:
            if direcao == 'D':
                Texto(win, x + 20, y + 5, palavra, cor, 10, 'bold')
                Reta(win, x, y, x - 12, y + 12, cor, 2, "continua")
                Ponto(win, x, y, 'red', 2)
                Reta(win, x - 12, y + 5, x - 3, y + 14, cor, 2, "continua")
                Reta(win, x + 3, y + 10, x - 10, y - 4, cor, 2, "continua")
            if direcao == 'P':
                Texto(win, x + 20, y + 5, palavra, cor, 10, 'bold')
                Reta(win, x, y, x - 12, y + 12, cor, 2, "continua")
                Ponto(win, x - 12, y + 12, 'red', 2)
                Reta(win, x - 12, y + 2, x - 1, y + 14, cor, 2, "continua")
                Reta(win, x, y + 5, x - 6, y - 1, cor, 2, "continua")
        if x < 0:
            if direcao == 'D':
                Texto(win, x + 20, y + 5, palavra, cor, 10, 'bold')
                Reta(win, x, y, x - 12, y + 12, cor, 2, "continua")
                Ponto(win, x - 12, y + 12, 'red', 2)
                Reta(win, x - 12, y + 2, x - 1, y + 14, cor, 2, "continua")
                Reta(win, x, y + 5, x - 6, y - 1, cor, 2, "continua")
            if direcao == 'P':
                Texto(win, x + 20, y + 5, palavra, cor, 10, 'bold')
                Reta(win, x, y, x - 12, y + 12, cor, 2, "continua")
                Ponto(win, x, y, 'red', 2)
                Reta(win, x - 12, y + 5, x - 3, y + 14, cor, 2, "continua")
                Reta(win, x + 3, y + 10, x - 10, y - 4, cor, 2, "continua")
        return
