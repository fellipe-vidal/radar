from .ponto import Ponto


def Circulo(win, x, y, r, cor, espessura):

    diametro = r * 2

    x1 = r - 1
    y1 = 0
    dx = 1
    dy = 1
    p = dx - diametro

    while x1 >= y1:
        Ponto(win, x1 + x, y1 + y, cor, espessura)
        Ponto(win, -x1 + x, y1 + y, cor, espessura)
        Ponto(win, -y1 + x, x1 + y, cor, espessura)
        Ponto(win, -x1 + x, -y1 + y, cor, espessura)
        Ponto(win, -y1 + x, -x1 + y, cor, espessura)
        Ponto(win, x1 + x, -y1 + y, cor, espessura)
        Ponto(win, y1 + x, -x1 + y, cor, espessura)
        Ponto(win, y1 + x, x1 + y, cor, espessura)

        if p <= 0:
            y1 += 1
            p += dy
            dy += 2

        if p > 0:
            x1 -= 1
            dx += 2
            p += (-diametro) + dx

    return
