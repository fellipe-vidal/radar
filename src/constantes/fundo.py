from ..primitivas.reta import Reta
from ..primitivas.circulo import Circulo
from ..primitivas.texto import Texto


def fundo(win):

    Reta(win, 360, 0, -360, 0, "gray", 2, "pontilhada")
    Reta(win, 0, 360, 0, -360, "gray", 2, "pontilhada")
    Circulo(win, 0, 0, 90, "gray", 1)
    Circulo(win, 0, 0, 180, "gray", 1)
    Circulo(win, 0, 0, 270, "gray", 1)
    Circulo(win, 0, 0, 360, "gray", 1)
    Reta(win, 260, -260, -260, 260, "gray", 1, "tracejada")
    Reta(win, -260, -260, 260, 260, "gray", 1, "tracejada")
    Reta(win, 360, 0, 380, 0, "gray", 2, "continua")
    Reta(win, -360, 0, -380, 0, "gray", 2, "continua")
    Reta(win, 0, 360, 0, 380, 'gray', 2, "continua")
    Reta(win, 0, -360, 0, -380, 'gray', 2, "continua")
    Reta(win, -255, -255, -270, -270, 'gray', 2, "continua")
    Reta(win, -255, 255, -270, 270, 'gray', 2, "continua")
    Reta(win, 255, 255, 270, 270, 'gray', 2, "continua")
    Reta(win, 255, -255, 270, -270, 'gray', 2, "continua")

    Reta(win, -310, -180, -330, -191, 'gray', 2, "continua")
    Reta(win, -310, 180, -330, 191, 'gray', 2, "continua")
    Reta(win, 310, -180, 330, -191, 'gray', 2, "continua")
    Reta(win, 310, 180, 330, 191, 'gray', 2, "continua")

    Reta(win, 350, 90, 370, 97, 'gray', 2, "continua")
    Reta(win, 350, -90, 370, -97, 'gray', 2, "continua")
    Reta(win, -350, 90, -370, 97, 'gray', 2, "continua")
    Reta(win, -350, -90, -370, -97, 'gray', 2, "continua")

    Reta(win, 170, 320, 179, 337, 'gray', 2, "continua")
    Reta(win, -170, 320, -179, 337, 'gray', 2, "continua")
    Reta(win, 170, -320, 179, -337, 'gray', 2, "continua")
    Reta(win, -170, -320, -179, -337, 'gray', 2, "continua")

    Reta(win, 100, 347, 105, 363, 'gray', 2, "continua")
    Reta(win, -100, 347, -105, 363, 'gray', 2, "continua")
    Reta(win, 100, -347, 105, -363, 'gray', 2, "continua")
    Reta(win, -100, -347, -105, -363, 'gray', 2, "continua")

    Texto(win, 340, 0, "90°", 'gray', 10, 'bold')
    Texto(win, 0, 340, "0°", 'gray', 10, 'bold')
    Texto(win, -340, 0, "270°", 'gray', 10, 'bold')
    Texto(win, 0, -340, "180°", 'gray', 10, 'bold')

    Texto(win, -290, -180, "240°", 'gray', 10, 'bold')
    Texto(win, -290, 180, "300°", 'gray', 10, 'bold')
    Texto(win, 290, -180, "120°", 'gray', 10, 'bold')
    Texto(win, 290, 180, "60°", 'gray', 10, 'bold')

    Texto(win, 330, 90, "75°", 'gray', 10, 'bold')
    Texto(win, -330, 90, "285°", 'gray', 10, 'bold')
    Texto(win, 330, -90, "105°", 'gray', 10, 'bold')
    Texto(win, -330, -90, "255°", 'gray', 10, 'bold')

    Texto(win, 170, 300, "30°", 'gray', 10, 'bold')
    Texto(win, -170, 300, "330°", 'gray', 10, 'bold')
    Texto(win, 170, -300, "150°", 'gray', 10, 'bold')
    Texto(win, -170, -300, "210°", 'gray', 10, 'bold')

    Texto(win, 100, 327, "15°", 'gray', 10, 'bold')
    Texto(win, -100, 327, "345°", 'gray', 10, 'bold')
    Texto(win, 100, -327, "165°", 'gray', 10, 'bold')
    Texto(win, -100, -327, "195°", 'gray', 10, 'bold')

    Texto(win, 240, 240, "45°", 'gray', 10, 'bold')
    Texto(win, -240, 240, "315°", 'gray', 10, 'bold')
    Texto(win, 240, -240, "135°", 'gray', 10, 'bold')
    Texto(win, -240, -240, "225°", 'gray', 10, 'bold')
