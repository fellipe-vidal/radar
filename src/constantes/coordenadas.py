coord = [
    {
        "LA 2203": ["P", -12990, 7500, 7500],
        "GZ 0331": ["P", -3473, 19696, 10000],
        "AZ 0032": ["P", -9397, -3420.201433, 5000],
        "AZ 0157": ["S", 0, 0, 0],
        "GZ 0667": ["S", 0, 0, 0],
    },
    {
        "LA 2203": ["P", -11951, 6900, 6900],
        "GZ 0331": ["P", -3212, 18219, 9250],
        "AZ 0032": ["P", -8457, -3078.18129, 4500],
        "AZ 0157": ["S", 0, 0, 0],
        "GZ 0667": ["S", 0, 0, 0],
    },
    {
        "LA 2203": ["P", -10912, 6300, 6300],
        "GZ 0331": ["P", -2952, 16742, 8500],
        "AZ 0032": ["P", -7518, -2736.161147, 4000],
        "AZ 0157": ["S", 0, 0, 0, ],
        "GZ 0667": ["S", 0, 0, 0, ],
    },
    {
        "LA 2203": ["P", -9873, 5700, 5700],
        "GZ 0331": ["P", -2692, 15265, 7750],
        "AZ 0032": ["P", -6578, -2394.141003, 3500],
        "AZ 0157": ["D", 866, 500, 500],
        "GZ 0667": ["S", 0, 0, 0],
    },
    {
        "LA 2203": ["P", -8833, 5100, 5100],
        "GZ 0331": ["P", -2431, 13787, 7000],
        "AZ 0032": ["P", -5638, -2052.12086, 3000],
        "AZ 0157": ["D", 1732, 1000, 1000],
        "GZ 0667": ["D", 500, 866, 500],
    },
    {
        "LA 2203": ["P", -7794, 4500, 4500],
        "GZ 0331": ["P", -2171, 12310, 6250],
        "AZ 0032": ["P", -4698, -1710.100717, 2500],
        "AZ 0157": ["D", 2598, 1500, 1500],
        "GZ 0667": ["D", 1050, 1819, 1050],
    },
    {
        "LA 2203": ["P", -6755, 3900, 3900],
        "GZ 0331": ["P", -1910, 10833, 5500],
        "AZ 0032": ["P", -3759, -1368.080573, 2000],
        "AZ 0157": ["D", 3464, 2000, 2000],
        "GZ 0667": ["D", 1600, 2771, 1600],
    },
    {
        "LA 2203": ["P", -5716, 3300, 3300],
        "GZ 0331": ["P", -1650, 9356, 4750],
        "AZ 0032": ["P", -2819, -1026.06043, 1500],
        "AZ 0157": ["D", 4330, 2500, 2500],
        "GZ 0667": ["D", 2150, 3724, 2150],
    },
    {
        "LA 2203": ["P", -4677, 2700, 2700],
        "GZ 0331": ["P", -1389, 7878, 4000],
        "AZ 0032": ["P", -1879, -684.0402867, 1000],
        "AZ 0157": ["D", 5196, 3000, 3000],
        "GZ 0667": ["D", 2700, 4677, 2700],
    },
    {
        "LA 2203": ["P", -3637, 2100, 2100],
        "GZ 0331": ["P", -1129, 6401, 3250],
        "AZ 0032": ["P", -940, -342.0201433, 500],
        "AZ 0157": ["D", 6062, 3500, 3500],
        "GZ 0667": ["D", 3250, 5629, 3250],
    },
    {
        "LA 2203": ["P", -2598, 1500, 1500],
        "GZ 0331": ["P", -868, 4924, 2500],
        "AZ 0032": ["P", 0, 0, 0],
        "AZ 0157": ["D", 6928, 4000, 4000],
        "GZ 0667": ["D", 3800, 6582, 3800],
    },
    {
        "LA 2203": ["P", -1559, 900, 900],
        "GZ 0331": ["P", -608, 3447, 1750],
        "AZ 0032": ["P", 0, 0, 0],
        "AZ 0157": ["D", 7794, 4500, 4500],
        "GZ 0667": ["D", 4350, 7534, 4350],
    },
    {
        "LA 2203": ["P", -520, 300, 300],
        "GZ 0331": ["P", -347, 1970, 1000],
        "AZ 0032": ["P", 0, 0, 0],
        "AZ 0157": ["D", 18660, 5000, 5000],
        "GZ 0667": ["D", 4900, 8487, 4900],
    },
    {
        "LA 2203": ["P", 0, 0, 0],
        "GZ 0331": ["P", -87, 492, 250],
        "AZ 0032": ["P", 0, 0, 0],
        "AZ 0157": ["D", 9526, 5500, 5500],
        "GZ 0667": ["D", 5450, 9440, 5450],
    },
    {
        "LA 2203": ["P", 0, 0, 0],
        "GZ 0331": ["P", 0, 0, 0],
        "AZ 0032": ["P", 0, 0, 0],
        "AZ 0157": ["D", 10392, 6000, 6000],
        "GZ 0667": ["D", 6000, 10392, 6000],
    },
    {
        "LA 2203": ["P", 0, 0, 0],
        "GZ 0331": ["P", 0, 0, 0],
        "AZ 0032": ["P", 0, 0, 0],
        "AZ 0157": ["D", 11258, 6500, 6500],
        "GZ 0667": ["D", 6550, 11345, 6550],
    },
]
